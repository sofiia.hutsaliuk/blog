require "test_helper"

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_authorization
    authorization = ArticlesController::HttpAuthentication::Basic.encode_credentials(users(:dhh).name, users(:dhh).password)
  
    get "/notes/1.xml", headers: { 'HTTP_AUTHORIZATION' => authorization }
  
    assert_equal 200, status
  end
end
